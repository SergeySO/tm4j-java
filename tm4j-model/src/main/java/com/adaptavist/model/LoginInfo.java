package com.adaptavist.model;

import java.util.Date;
import java.util.Objects;

/**
 * The model object that stores information about Login Info
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class LoginInfo {
    private int loginCount;
    private int failedLoginCount;
    private Date previousLoginTime;
    private Date lastFailedLoginTime;

    /**
     * Gets loginCount.
     *
     * @return the loginCount
     */
    public int getLoginCount() {
        return loginCount;
    }

    /**
     * Sets loginCount.
     *
     * @param loginCount the loginCount
     * @return self for method chaining.
     */
    public LoginInfo setLoginCount(int loginCount) {
        this.loginCount = loginCount;
        return this;
    }

    /**
     * Gets failedLoginCount.
     *
     * @return the failedLoginCount
     */
    public int getFailedLoginCount() {
        return failedLoginCount;
    }

    /**
     * Sets failedLoginCount.
     *
     * @param failedLoginCount the failedLoginCount
     * @return self for method chaining.
     */
    public LoginInfo setFailedLoginCount(int failedLoginCount) {
        this.failedLoginCount = failedLoginCount;
        return this;
    }

    /**
     * Gets previousLoginTime.
     *
     * @return the previousLoginTime
     */
    public Date getPreviousLoginTime() {
        return previousLoginTime;
    }

    /**
     * Sets previousLoginTime.
     *
     * @param previousLoginTime the previousLoginTime
     * @return self for method chaining.
     */
    public LoginInfo setPreviousLoginTime(Date previousLoginTime) {
        this.previousLoginTime = previousLoginTime;
        return this;
    }

    /**
     * Gets lastFailedLoginTime.
     *
     * @return the lastFailedLoginTime
     */
    public Date getLastFailedLoginTime() {
        return lastFailedLoginTime;
    }

    /**
     * Sets lastFailedLoginTime.
     *
     * @param lastFailedLoginTime the lastFailedLoginTime
     * @return self for method chaining.
     */
    public LoginInfo setLastFailedLoginTime(Date lastFailedLoginTime) {
        this.lastFailedLoginTime = lastFailedLoginTime;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.LoginInfo loginInfo = (com.adaptavist.model.LoginInfo) o;
        return loginCount == loginInfo.loginCount &&
                failedLoginCount == loginInfo.failedLoginCount &&
                Objects.equals(previousLoginTime, loginInfo.previousLoginTime) &&
                Objects.equals(lastFailedLoginTime, loginInfo.lastFailedLoginTime);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(loginCount, failedLoginCount, previousLoginTime, lastFailedLoginTime);
    }
}
