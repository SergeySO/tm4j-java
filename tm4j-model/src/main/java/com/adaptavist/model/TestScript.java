package com.adaptavist.model;

import java.util.List;
import java.util.Objects;

/**
 * @author Osinnikov S.S. on 26.12.2021
 */
public class TestScript {
    private String type;
    private List<Step> steps;

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     * @return self for method chaining.
     */
    public TestScript setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * Gets steps.
     *
     * @return the steps
     */
    public List<Step> getSteps() {
        return steps;
    }

    /**
     * Sets steps.
     *
     * @param steps the steps
     * @return self for method chaining.
     */
    public TestScript setSteps(List<Step> steps) {
        this.steps = steps;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TestScript that = (TestScript) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(steps, that.steps);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, steps);
    }
}
