package com.adaptavist.model;

import java.util.Objects;

/**
 * The model object that stores information about Credential
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class Credential {
    private String username;
    private String password;

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     * @return self for method chaining.
     */
    public Credential setUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     * @return self for method chaining.
     */
    public Credential setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.Credential that = (com.adaptavist.model.Credential) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}
