package com.adaptavist.model;

/**
 * Jira case statuses
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public enum JiraCaseStatus {
    PASS("Pass"),
    FAILED("Fail"),
    NOT_EXECUTED("Not Executed");

    private final String value;

    JiraCaseStatus(final String value) {
        this.value = value;
    }

    /**
     * From value status.
     *
     * @param value the value
     * @return the status
     */
    public static JiraCaseStatus fromValue(final String value) {
        for (JiraCaseStatus status: JiraCaseStatus.values()) {
            if (status.value.equals(value)) {
                return status;
            }
        }

        throw new IllegalArgumentException(value);
    }

    /**
     * Value string.
     *
     * @return the string
     */
    public String value() {
        return value;
    }
}
