package com.adaptavist.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The model object that stores information about test run that need to create for Jira
 *
 * Examples:
 * 1. Simple request, using only required fields
 * {
 * "name": "Full regression",
 * "projectKey": "JQA"
 * }
 *
 * 2. Simple request, using custom fields
 * {
 *   "projectKey": "JQA",
 *   "name": "Full regression",
 *   "customFields": {
 *     "single choice": "Propulsion engines",
 *     "multichoice": "Brazil, England",
 *     "checkbox": "false",
 *     "datepicker": "2017-03-02T16:15:00.000Z",
 *     "decimal number": 12.12,
 *     "number": 100,
 *     "multi line text": "<h1>Ensure the axial-flow pump is enabled</h1><p>Ignite the secondary propulsion engines</p>",
 *     "single line": "Ignite the secondary propulsion engines"
 *   }
 * }
 *
 * 3. Full request
 * {
 *   "projectKey": "JQA",
 *   "testPlanKey": "JQA-P123",
 *   "name": "Full regression",
 *   "status": "In Progress",
 *   "iteration": "Sprint 1",
 *   "version": "1.1.0",
 *   "folder": "/root folder/child folder",
 *   "issueKey": "JQA-123",
 *   "owner": "admin",
 *   "plannedStartDate": "2016-01-11T00:00:00-0300",
 *   "plannedEndDate": "2016-04-13T00:00:00-0300",
 *   "customFields": {
 *     "single choice": "Propulsion engines",
 *     "multichoice": "Brazil, England"
 *   },
 *   "items": [
 *     {
 *       "testCaseKey": "JQA-T123",
 *       "status": "Pass",
 *       "environment": "Chrome",
 *       "comment": "The test has failed on some automation tool procedure.",
 *       "userKey": "vitor.pelizza",
 *       "executionTime": 180000,
 *       "executionDate": "2016-01-30T14:54:00Z",
 *       "customFields": {
 *         "single choice": "Propulsion engines",
 *         "multichoice": "Brazil, England"
 *       },
 *       "scriptResults": [
 *         {
 *           "index": 0,
 *           "status": "Fail",
 *           "comment": "This step has failed."
 *         }
 *       ]
 *     },
 *     {
 *       "testCaseKey": "JQA-T456"
 *     }
 *   ]
 * }
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class TestRunRq {
    private String name;
    private String projectKey;
    private String testPlanKey;
    private String status;
    private String iteration;
    private String version;
    private String folder;
    private String issueKey;
    private String owner;
    private String plannedStartDate;
    private String plannedEndDate;
    private List<Item> items;
    private Map<String, Object> customFields;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     * @return self for method chaining.
     */
    public TestRunRq setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Gets projectKey.
     *
     * @return the projectKey
     */
    public String getProjectKey() {
        return projectKey;
    }

    /**
     * Sets projectKey.
     *
     * @param projectKey the projectKey
     * @return self for method chaining.
     */
    public TestRunRq setProjectKey(String projectKey) {
        this.projectKey = projectKey;
        return this;
    }

    /**
     * Gets testPlanKey.
     *
     * @return the testPlanKey
     */
    public String getTestPlanKey() {
        return testPlanKey;
    }

    /**
     * Sets testPlanKey.
     *
     * @param testPlanKey the testPlanKey
     * @return self for method chaining.
     */
    public TestRunRq setTestPlanKey(String testPlanKey) {
        this.testPlanKey = testPlanKey;
        return this;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     * @return self for method chaining.
     */
    public TestRunRq setStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * Gets iteration.
     *
     * @return the iteration
     */
    public String getIteration() {
        return iteration;
    }

    /**
     * Sets iteration.
     *
     * @param iteration the iteration
     * @return self for method chaining.
     */
    public TestRunRq setIteration(String iteration) {
        this.iteration = iteration;
        return this;
    }

    /**
     * Gets version.
     *
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets version.
     *
     * @param version the version
     * @return self for method chaining.
     */
    public TestRunRq setVersion(String version) {
        this.version = version;
        return this;
    }

    /**
     * Gets folder.
     *
     * @return the folder
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Sets folder.
     *
     * @param folder the folder
     * @return self for method chaining.
     */
    public TestRunRq setFolder(String folder) {
        this.folder = folder;
        return this;
    }

    /**
     * Gets issueKey.
     *
     * @return the issueKey
     */
    public String getIssueKey() {
        return issueKey;
    }

    /**
     * Sets issueKey.
     *
     * @param issueKey the issueKey
     * @return self for method chaining.
     */
    public TestRunRq setIssueKey(String issueKey) {
        this.issueKey = issueKey;
        return this;
    }

    /**
     * Gets owner.
     *
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets owner.
     *
     * @param owner the owner
     * @return self for method chaining.
     */
    public TestRunRq setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Gets plannedStartDate.
     *
     * @return the plannedStartDate
     */
    public String getPlannedStartDate() {
        return plannedStartDate;
    }

    /**
     * Sets plannedStartDate.
     *
     * @param plannedStartDate the plannedStartDate
     * @return self for method chaining.
     */
    public TestRunRq setPlannedStartDate(String plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
        return this;
    }

    /**
     * Gets plannedEndDate.
     *
     * @return the plannedEndDate
     */
    public String getPlannedEndDate() {
        return plannedEndDate;
    }

    /**
     * Sets plannedEndDate.
     *
     * @param plannedEndDate the plannedEndDate
     * @return self for method chaining.
     */
    public TestRunRq setPlannedEndDate(String plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
        return this;
    }

    /**
     * Gets items.
     *
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * Sets items.
     *
     * @param items the items
     * @return self for method chaining.
     */
    public TestRunRq setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    public Map<String, Object> getCustomFields() {
        if (customFields == null) {
            customFields = new HashMap<>();
        }

        return customFields;
    }

    /**
     * Sets customFields.
     *
     * @param customFields the customFields
     * @return self for method chaining.
     */
    public TestRunRq setCustomFields(Map<String, Object> customFields) {
        this.customFields = customFields;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.TestRunRq testRunRq = (com.adaptavist.model.TestRunRq) o;
        return Objects.equals(name, testRunRq.name) &&
                Objects.equals(projectKey, testRunRq.projectKey) &&
                Objects.equals(testPlanKey, testRunRq.testPlanKey) &&
                Objects.equals(status, testRunRq.status) &&
                Objects.equals(iteration, testRunRq.iteration) &&
                Objects.equals(version, testRunRq.version) &&
                Objects.equals(folder, testRunRq.folder) &&
                Objects.equals(issueKey, testRunRq.issueKey) &&
                Objects.equals(owner, testRunRq.owner) &&
                Objects.equals(plannedStartDate, testRunRq.plannedStartDate) &&
                Objects.equals(plannedEndDate, testRunRq.plannedEndDate) &&
                Objects.equals(items, testRunRq.items) &&
                Objects.equals(customFields, testRunRq.customFields);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, projectKey, testPlanKey, status, iteration, version,
                folder, issueKey, owner, plannedStartDate, plannedEndDate, items, customFields);
    }
}
