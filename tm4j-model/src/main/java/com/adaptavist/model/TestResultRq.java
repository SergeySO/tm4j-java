package com.adaptavist.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The model object that stores information about test result that need to create result for Jira
 *
 * Examples:
 * 1. Simple request, using only required fields:
 * {
 *   "projectKey": "JQA",
 *   "testCaseKey": "JQA-T123"
 * }
 *
 * 2. Full request
 * {
 *   "projectKey": "JQA",
 *   "testCaseKey": "JQA-T123",
 *   "status": "Fail",
 *   "environment": "Firefox",
 *   "comment": "The test has failed on some automation tool procedure.",
 *   "assignedTo": "vitor.pelizza",
 *   "executedBy": "cristiano.caetano",
 *   "executionTime": 180000,
 *   "actualStartDate": "2016-02-14T19:22:00-0300",
 *   "actualEndDate": "2016-02-15T19:22:00-0300",
 *   "customFields": {
 *     "CI Server": "Bamboo"
 *   },
 *   "issueLinks": ["JQA-123", "JQA-456"],
 *   "scriptResults": [
 *     {
 *       "index": 0,
 *       "status": "Fail",
 *       "comment": "This step has failed."
 *     }
 *   ]
 * }
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class TestResultRq {
    private String executedBy;
    private long executionTime;
    private String actualEndDate;
    private String environment;
    private List<String> issueLinks;
    private String actualStartDate;
    private List<ScriptResult> scriptResults;
    private String comment;
    private String assignedTo;
    private String status;
    private Map<String, Object> customFields;

    /**
     * Gets executedBy.
     *
     * @return the executedBy
     */
    public String getExecutedBy() {
        return executedBy;
    }

    /**
     * Sets executedBy.
     *
     * @param executedBy the executedBy
     * @return self for method chaining.
     */
    public TestResultRq setExecutedBy(String executedBy) {
        this.executedBy = executedBy;
        return this;
    }

    /**
     * Gets executionTime.
     *
     * @return the executionTime
     */
    public long getExecutionTime() {
        return executionTime;
    }

    /**
     * Sets executionTime.
     *
     * @param executionTime the executionTime
     * @return self for method chaining.
     */
    public TestResultRq setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
        return this;
    }

    /**
     * Gets actualEndDate.
     *
     * @return the actualEndDate
     */
    public String getActualEndDate() {
        return actualEndDate;
    }

    /**
     * Sets actualEndDate.
     *
     * @param actualEndDate the actualEndDate
     * @return self for method chaining.
     */
    public TestResultRq setActualEndDate(String actualEndDate) {
        this.actualEndDate = actualEndDate;
        return this;
    }

    /**
     * Gets environment.
     *
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * Sets environment.
     *
     * @param environment the environment
     * @return self for method chaining.
     */
    public TestResultRq setEnvironment(String environment) {
        this.environment = environment;
        return this;
    }

    /**
     * Gets issueLinks.
     *
     * @return the issueLinks
     */
    public List<String> getIssueLinks() {
        return issueLinks;
    }

    /**
     * Sets issueLinks.
     *
     * @param issueLinks the issueLinks
     * @return self for method chaining.
     */
    public TestResultRq setIssueLinks(List<String> issueLinks) {
        this.issueLinks = issueLinks;
        return this;
    }

    /**
     * Gets actualStartDate.
     *
     * @return the actualStartDate
     */
    public String getActualStartDate() {
        return actualStartDate;
    }

    /**
     * Sets actualStartDate.
     *
     * @param actualStartDate the actualStartDate
     * @return self for method chaining.
     */
    public TestResultRq setActualStartDate(String actualStartDate) {
        this.actualStartDate = actualStartDate;
        return this;
    }

    /**
     * Gets scriptResults.
     *
     * @return the scriptResults
     */
    public List<ScriptResult> getScriptResults() {
        return scriptResults;
    }

    /**
     * Sets scriptResults.
     *
     * @param scriptResults the scriptResults
     * @return self for method chaining.
     */
    public TestResultRq setScriptResults(List<ScriptResult> scriptResults) {
        this.scriptResults = scriptResults;
        return this;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     * @return self for method chaining.
     */
    public TestResultRq setComment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * Gets assignedTo.
     *
     * @return the assignedTo
     */
    public String getAssignedTo() {
        return assignedTo;
    }

    /**
     * Sets assignedTo.
     *
     * @param assignedTo the assignedTo
     * @return self for method chaining.
     */
    public TestResultRq setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
        return this;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     * @return self for method chaining.
     */
    public TestResultRq setStatus(String status) {
        this.status = status;
        return this;
    }

    public Map<String, Object> getCustomFields() {
        if (customFields == null) {
            customFields = new HashMap<>();
        }

        return customFields;
    }

    /**
     * Sets customFields.
     *
     * @param customFields the customFields
     * @return self for method chaining.
     */
    public TestResultRq setCustomFields(Map<String, Object> customFields) {
        this.customFields = customFields;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.TestResultRq that = (com.adaptavist.model.TestResultRq) o;
        return executionTime == that.executionTime &&
                Objects.equals(executedBy, that.executedBy) &&
                Objects.equals(actualEndDate, that.actualEndDate) &&
                Objects.equals(environment, that.environment) &&
                Objects.equals(issueLinks, that.issueLinks) &&
                Objects.equals(actualStartDate, that.actualStartDate) &&
                Objects.equals(scriptResults, that.scriptResults) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(assignedTo, that.assignedTo) &&
                Objects.equals(status, that.status) &&
                Objects.equals(customFields, that.customFields);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(executedBy, executionTime, actualEndDate, environment, issueLinks,
                actualStartDate, scriptResults, comment, assignedTo, status, customFields);
    }
}
