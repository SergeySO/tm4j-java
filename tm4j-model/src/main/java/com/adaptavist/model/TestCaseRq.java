package com.adaptavist.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The model object that stores information about test case that need to create for Jira
 *
 * Examples:
 * 1. Simple request, using only required fields
 *{
 *   "name": "Full regression",
 *   "projectKey": "JQA"
 * }
 *
 * 2. Simple request, using only required fields and a step by step test script
 * {
 *   "projectKey": "JQA",
 *   "name": "Ensure the axial-flow pump is enabled",
 *   "testScript": {
 *     "type": "STEP_BY_STEP",
 *     "steps": [
 *       {
 *         "description": "Ignite the secondary propulsion engines.",
 *         "testData": "Combustion chamber's initial pressure: 10",
 *         "expectedResult": "Ensure the high-pressure combustion chamber's pressure is around 3000 psi."
 *       }
 *     ]
 *   }
 * }
 *
 * 3. Simple request, using only required fields and a plain text test script
 * {
 *   "projectKey": "JQA",
 *   "name": "Ensure the axial-flow pump is enabled",
 *   "testScript": {
 *     "type": "PLAIN_TEXT",
 *     "text": "Ignite the secondary propulsion engines and ensure the high-pressure combustion chamber's pressure is around 3000 psi."
 *   }
 * }
 *
 * 4. Simple request, using custom fields
 * {
 *   "projectKey": "JQA",
 *   "name": "Ensure the axial-flow pump is enabled",
 *   "customFields": {
 *     "single choice": "Propulsion engines",
 *     "multichoice": "Brazil, England",
 *     "checkbox": "false",
 *     "datepicker": "2017-03-02T16:15:00.000Z",
 *     "decimal number": 12.12,
 *     "number": 100,
 *     "multi line text": "<h1>Ensure the axial-flow pump is enabled</h1><p>Ignite the secondary propulsion engines</p>",
 *     "single line": "Ignite the secondary propulsion engines"
 *   }
 * }
 *
 * 5. Simple request, with call to tests
 * {
 *   "projectKey": "JQA",
 *   "name": "Ensure the axial-flow pump is enabled",
 *   "testScript": {
 *     "type": "STEP_BY_STEP",
 *     "steps": [
 *       {
 *         "testCaseKey": "JQA-123"
 *       },
 *       {
 *         "description": "Ignite the secondary propulsion engines.",
 *         "testData": "Combustion chamber's initial pressure: 10",
 *         "expectedResult": "Ensure the high-pressure combustion chamber's pressure is around 3000 psi."
 *       },
 *       {
 *         "testCaseKey": "JQA-789"
 *       }
 *     ]
 *   }
 * }
 *
 * 6. Full request
 * {
 *   "projectKey": "JQA",
 *   "name": "Ensure the axial-flow pump is enabled",
 *   "precondition": "The precondition.",
 *   "objective": "The objective.",
 *   "folder": "/Orbiter/Cargo Bay",
 *   "status": "Approved",
 *   "priority": "Low",
 *   "component": "Valves",
 *   "owner": "vitor.pelizza",
 *   "estimatedTime": 138000,
 *   "labels": ["Smoke", "Functional"],
 *   "issueLinks": ["JQA-123", "JQA-456"],
 *   "customFields": {
 *     "single choice": "Propulsion engines",
 *     "multichoice": "Brazil, England"
 *   },
 *   "testScript": {
 *     "type": "STEP_BY_STEP",
 *     "steps": [
 *       {
 *         "description": "Ignite the secondary propulsion engines.",
 *         "testData": "Combustion chamber's initial pressure: 10",
 *         "expectedResult": "Ensure the high-pressure combustion chamber's pressure is around 3000 psi."
 *       }
 *     ]
 *   }
 * }
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class TestCaseRq {
    private String projectKey;
    private String name;
    private String precondition;
    private String objective;
    private String folder;
    private String status;
    private String priority;
    private String component;
    private String owner;
    private String estimatedTime;
    private List<String> labels;
    private List<String> issueLinks;
    private TestScript testScript;
    private Map<String, Object> customFields;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     * @return self for method chaining.
     */
    public TestCaseRq setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Gets projectKey.
     *
     * @return the projectKey
     */
    public String getProjectKey() {
        return projectKey;
    }

    /**
     * Sets projectKey.
     *
     * @param projectKey the projectKey
     * @return self for method chaining.
     */
    public TestCaseRq setProjectKey(String projectKey) {
        this.projectKey = projectKey;
        return this;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     * @return self for method chaining.
     */
    public TestCaseRq setStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * Gets folder.
     *
     * @return the folder
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Sets folder.
     *
     * @param folder the folder
     * @return self for method chaining.
     */
    public TestCaseRq setFolder(String folder) {
        this.folder = folder;
        return this;
    }

    /**
     * Gets owner.
     *
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets owner.
     *
     * @param owner the owner
     * @return self for method chaining.
     */
    public TestCaseRq setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Gets precondition.
     *
     * @return the precondition
     */
    public String getPrecondition() {
        return precondition;
    }

    /**
     * Sets precondition.
     *
     * @param precondition the precondition
     * @return self for method chaining.
     */
    public TestCaseRq setPrecondition(String precondition) {
        this.precondition = precondition;
        return this;
    }

    /**
     * Gets objective.
     *
     * @return the objective
     */
    public String getObjective() {
        return objective;
    }

    /**
     * Sets objective.
     *
     * @param objective the objective
     * @return self for method chaining.
     */
    public TestCaseRq setObjective(String objective) {
        this.objective = objective;
        return this;
    }

    /**
     * Gets priority.
     *
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets priority.
     *
     * @param priority the priority
     * @return self for method chaining.
     */
    public TestCaseRq setPriority(String priority) {
        this.priority = priority;
        return this;
    }

    /**
     * Gets component.
     *
     * @return the component
     */
    public String getComponent() {
        return component;
    }

    /**
     * Sets component.
     *
     * @param component the component
     * @return self for method chaining.
     */
    public TestCaseRq setComponent(String component) {
        this.component = component;
        return this;
    }

    /**
     * Gets estimatedTime.
     *
     * @return the estimatedTime
     */
    public String getEstimatedTime() {
        return estimatedTime;
    }

    /**
     * Sets estimatedTime.
     *
     * @param estimatedTime the estimatedTime
     * @return self for method chaining.
     */
    public TestCaseRq setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
        return this;
    }

    /**
     * Gets labels.
     *
     * @return the labels
     */
    public List<String> getLabels() {
        return labels;
    }

    /**
     * Sets labels.
     *
     * @param labels the labels
     * @return self for method chaining.
     */
    public TestCaseRq setLabels(List<String> labels) {
        this.labels = labels;
        return this;
    }

    /**
     * Gets issueLinks.
     *
     * @return the issueLinks
     */
    public List<String> getIssueLinks() {
        return issueLinks;
    }

    /**
     * Sets issueLinks.
     *
     * @param issueLinks the issueLinks
     * @return self for method chaining.
     */
    public TestCaseRq setIssueLinks(List<String> issueLinks) {
        this.issueLinks = issueLinks;
        return this;
    }

    /**
     * Gets testScript.
     *
     * @return the testScript
     */
    public TestScript getTestScript() {
        return testScript;
    }

    /**
     * Sets testScript.
     *
     * @param testScript the testScript
     * @return self for method chaining.
     */
    public TestCaseRq setTestScript(TestScript testScript) {
        this.testScript = testScript;
        return this;
    }

    /**
     * Gets customFields.
     *
     * @return the customFields
     */
    public Map<String, Object> getCustomFields() {
        if (customFields == null) {
            customFields = new HashMap<>();
        }

        return customFields;
    }

    /**
     * Sets customFields.
     *
     * @param customFields the customFields
     * @return self for method chaining.
     */
    public TestCaseRq setCustomFields(Map<String, Object> customFields) {
        this.customFields = customFields;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final TestCaseRq that = (TestCaseRq) o;
        return Objects.equals(projectKey, that.projectKey) &&
                Objects.equals(name, that.name) &&
                Objects.equals(precondition, that.precondition) &&
                Objects.equals(objective, that.objective) &&
                Objects.equals(folder, that.folder) &&
                Objects.equals(status, that.status) &&
                Objects.equals(priority, that.priority) &&
                Objects.equals(component, that.component) &&
                Objects.equals(owner, that.owner) &&
                Objects.equals(estimatedTime, that.estimatedTime) &&
                Objects.equals(labels, that.labels) &&
                Objects.equals(issueLinks, that.issueLinks) &&
                Objects.equals(customFields, that.customFields) &&
                Objects.equals(testScript, that.testScript);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectKey, name, precondition, objective, folder, status, priority,
                component, owner, estimatedTime, labels, issueLinks, testScript, customFields);
    }
}
