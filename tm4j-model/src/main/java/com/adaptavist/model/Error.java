package com.adaptavist.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The model object that stores information about error
 *
 * @author Osinnikov S.S. on 28.12.2021
 */
public class Error {
    private List<String> errorMessages;

    /**
     * Gets errorMessages.
     *
     * @return the errorMessages
     */
    public List<String> getErrorMessages() {
        if (errorMessages == null) {
            errorMessages = new ArrayList<>();
        }

        return errorMessages;
    }

    /**
     * Sets errorMessages.
     *
     * @param errorMessages the errorMessages
     * @return self for method chaining.
     */
    public Error setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Error error = (Error) o;
        return Objects.equals(errorMessages, error.errorMessages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorMessages);
    }
}
