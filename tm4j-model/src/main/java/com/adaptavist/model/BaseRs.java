package com.adaptavist.model;

import java.util.Objects;

/**
 * @author Osinnikov S.S. on 26.12.2021
 */
public class BaseRs {
    private Integer id;
    private String self;
    private String key;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     * @return self for method chaining.
     */
    public BaseRs setId(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Gets self.
     *
     * @return the self
     */
    public String getSelf() {
        return self;
    }

    /**
     * Sets self.
     *
     * @param self the self
     * @return self for method chaining.
     */
    public BaseRs setSelf(String self) {
        this.self = self;
        return this;
    }

    /**
     * Gets key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets key.
     *
     * @param key the key
     * @return self for method chaining.
     */
    public BaseRs setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.BaseRs baseRs = (com.adaptavist.model.BaseRs) o;
        return Objects.equals(id, baseRs.id) &&
                Objects.equals(self, baseRs.self) &&
                Objects.equals(key, baseRs.key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, self, key);
    }
}
