package com.adaptavist.model;

import java.util.Objects;

/**
 * The model object that stores information about Session
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class Session {
    private String name;
    private String value;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     * @return self for method chaining.
     */
    public Session setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     * @return self for method chaining.
     */
    public Session setValue(String value) {
        this.value = value;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.Session session = (com.adaptavist.model.Session) o;
        return Objects.equals(name, name) &&
                Objects.equals(value, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, value);
    }
}
