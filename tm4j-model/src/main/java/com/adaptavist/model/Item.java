package com.adaptavist.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The model object that stores information about item
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class Item {
    private String testCaseKey;
    private JiraCaseStatus status;
    private String environment;
    private String comment;
    private String userKey;
    private String executionTime;
    private String executionDate;
    private List<ScriptResult> scriptResults;
    private Map<String, Object> customFields;

    /**
     * Gets testCaseKey.
     *
     * @return the testCaseKey
     */
    public String getTestCaseKey() {
        return testCaseKey;
    }

    /**
     * Sets testCaseKey.
     *
     * @param testCaseKey the testCaseKey
     * @return self for method chaining.
     */
    public Item setTestCaseKey(String testCaseKey) {
        this.testCaseKey = testCaseKey;
        return this;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public JiraCaseStatus getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     * @return self for method chaining.
     */
    public Item setStatus(JiraCaseStatus status) {
        this.status = status;
        return this;
    }

    /**
     * Gets environment.
     *
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * Sets environment.
     *
     * @param environment the environment
     * @return self for method chaining.
     */
    public Item setEnvironment(String environment) {
        this.environment = environment;
        return this;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     * @return self for method chaining.
     */
    public Item setComment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * Gets userKey.
     *
     * @return the userKey
     */
    public String getUserKey() {
        return userKey;
    }

    /**
     * Sets userKey.
     *
     * @param userKey the userKey
     * @return self for method chaining.
     */
    public Item setUserKey(String userKey) {
        this.userKey = userKey;
        return this;
    }

    /**
     * Gets executionTime.
     *
     * @return the executionTime
     */
    public String getExecutionTime() {
        return executionTime;
    }

    /**
     * Sets executionTime.
     *
     * @param executionTime the executionTime
     * @return self for method chaining.
     */
    public Item setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
        return this;
    }

    /**
     * Gets executionDate.
     *
     * @return the executionDate
     */
    public String getExecutionDate() {
        return executionDate;
    }

    /**
     * Sets executionDate.
     *
     * @param executionDate the executionDate
     * @return self for method chaining.
     */
    public Item setExecutionDate(String executionDate) {
        this.executionDate = executionDate;
        return this;
    }

    /**
     * Gets scriptResults.
     *
     * @return the scriptResults
     */
    public List<ScriptResult> getScriptResults() {
        return scriptResults;
    }

    /**
     * Sets scriptResults.
     *
     * @param scriptResults the scriptResults
     * @return self for method chaining.
     */
    public Item setScriptResults(List<ScriptResult> scriptResults) {
        this.scriptResults = scriptResults;
        return this;
    }

    /**
     * Gets customFields.
     *
     * @return the customFields
     */
    public Map<String, Object> getCustomFields() {
        if (customFields == null) {
            customFields = new HashMap<>();
        }

        return customFields;
    }

    /**
     * Sets customFields.
     *
     * @param customFields the customFields
     * @return self for method chaining.
     */
    public Item setCustomFields(Map<String, Object> customFields) {
        this.customFields = customFields;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.Item item = (com.adaptavist.model.Item) o;
        return Objects.equals(testCaseKey, item.testCaseKey) &&
                status == item.status &&
                Objects.equals(environment, item.environment) &&
                Objects.equals(comment, item.comment) &&
                Objects.equals(userKey, item.userKey) &&
                Objects.equals(executionTime, item.executionTime) &&
                Objects.equals(executionDate, item.executionDate) &&
                Objects.equals(scriptResults, item.scriptResults) &&
                Objects.equals(customFields, item.customFields);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(testCaseKey, status, environment, comment, userKey,
                executionTime, executionDate, scriptResults, customFields);
    }
}
