package com.adaptavist.model;

import java.util.Objects;

/**
 * The model object that stores information about script result
 *
 * Example:
 *
 * "scriptResults": [
 *   {
 *     "index": 0,
 *     "status": "Fail",
 *     "comment": "This step has failed."
 *   }
 * ]
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class ScriptResult {
    private int index;
    private String status;
    private String comment;

    /**
     * Gets index.
     *
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets index.
     *
     * @param index the index
     * @return self for method chaining.
     */
    public ScriptResult setIndex(int index) {
        this.index = index;
        return this;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     * @return self for method chaining.
     */
    public ScriptResult setStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     * @return self for method chaining.
     */
    public ScriptResult setComment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final com.adaptavist.model.ScriptResult that = (com.adaptavist.model.ScriptResult) o;
        return index == that.index &&
                Objects.equals(status, that.status) &&
                Objects.equals(comment, that.comment);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(index, status, comment);
    }
}
