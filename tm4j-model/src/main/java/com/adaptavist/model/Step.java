package com.adaptavist.model;

import java.util.Objects;

/**
 * The model object that stores information about steps
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class Step {
    private String description;
    private String testData;
    private String expectedResult;

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     * @return self for method chaining.
     */
    public Step setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Gets testData.
     *
     * @return the testData
     */
    public String getTestData() {
        return testData;
    }

    /**
     * Sets testData.
     *
     * @param testData the testData
     * @return self for method chaining.
     */
    public Step setTestData(String testData) {
        this.testData = testData;
        return this;
    }

    /**
     * Gets expectedResult.
     *
     * @return the expectedResult
     */
    public String getExpectedResult() {
        return expectedResult;
    }

    /**
     * Sets expectedResult.
     *
     * @param expectedResult the expectedResult
     * @return self for method chaining.
     */
    public Step setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Step step = (Step) o;
        return Objects.equals(description, step.description) &&
                Objects.equals(testData, step.testData) &&
                Objects.equals(expectedResult, step.expectedResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, testData, expectedResult);
    }
}
