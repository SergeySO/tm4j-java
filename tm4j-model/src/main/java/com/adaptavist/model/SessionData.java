package com.adaptavist.model;

import java.util.Objects;

/**
 * The model object that stores information about Session data
 *
 * @author Osinnikov S.S. on 26.12.2021
 */
public class SessionData {
    private Session session;
    private LoginInfo loginInfo;

    /**
     * Gets 
     *
     * @return the session
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets 
     *
     * @param session the session
     * @return self for method chaining.
     */
    public SessionData setSession(Session session) {
        this.session = session;
        return this;
    }

    /**
     * Gets loginInfo.
     *
     * @return the loginInfo
     */
    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    /**
     * Sets loginInfo.
     *
     * @param loginInfo the loginInfo
     * @return self for method chaining.
     */
    public SessionData setLoginInfo(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final SessionData that = (SessionData) o;
        return Objects.equals(session, that.session) &&
                Objects.equals(loginInfo, that.loginInfo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(session, loginInfo);
    }
}
