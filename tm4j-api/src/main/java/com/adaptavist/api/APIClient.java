package com.adaptavist.api;

import com.adaptavist.api.helper.GsonHelper;
import com.adaptavist.api.utils.SSLUtils;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.net.ssl.X509TrustManager;
import java.util.concurrent.TimeUnit;

/**
 * @author Osinnikov S.S. on 28.12.2021
 */
public class APIClient {
    private static Retrofit retrofit = null;

    static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(getOkHttpClientBuilderWithDefaultParams()
                            .addInterceptor(interceptor)
                            .build())
                    .addConverterFactory(GsonConverterFactory
                            .create(GsonHelper.getGson()))
                    .build();
        }

        return retrofit;
    }

    private static OkHttpClient.Builder getOkHttpClientBuilderWithDefaultParams() {
        return new OkHttpClient().newBuilder()
                .cookieJar(new JavaNetCookieJar())
                .addInterceptor(new HttpLoggingInterceptor())
                .sslSocketFactory(SSLUtils.createSocketFactory(), (X509TrustManager) SSLUtils.getTrustManagers()[0])
                .hostnameVerifier((s, sslSession) -> true)
                .connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
    }
}
