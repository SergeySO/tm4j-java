package com.adaptavist.api;

import com.adaptavist.model.TestCaseRq;
import com.adaptavist.model.TestCaseRs;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;

/**
 * @author Osinnikov S.S. on 27.12.2021
 */
public interface TestCaseService {
    @GET("atm/1.0/testcase")
    Call<TestCaseRs> createTestCase(@Body TestCaseRq testCaseRq);
}
