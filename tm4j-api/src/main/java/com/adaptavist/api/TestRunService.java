package com.adaptavist.api;

import com.adaptavist.model.TestRunRq;
import com.adaptavist.model.TestRunRs;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Osinnikov S.S. on 27.12.2021
 */
public interface TestRunService {
    @POST("atm/1.0/testrun")
    Call<TestRunRs> createTestRun(@Body TestRunRq testRunRq);
}
