package com.adaptavist.api.utils;

import com.adaptavist.api.secure.TrustAllManager;
import com.adaptavist.api.utils.exeption.WTFException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author Osinnikov S.S. on 16.11.2021
 */
public class SSLUtils {
    public static SSLSocketFactory createDefaultSocketFactory() {
        return (SSLSocketFactory) SSLSocketFactory.getDefault();
    }

    public static SSLSocketFactory createSocketFactory() {
        return createSSLContext(getTrustManagers()).getSocketFactory();
    }

    public static SSLContext createSSLContext(TrustManager[] trustManagers) {
        SSLContext sslContext;

        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagers, new SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            throw new WTFException("I nothing found", e);
        } catch (KeyManagementException e) {
            throw new WTFException("SSLContext is always created", e);
        }

        return sslContext;
    }

    public static TrustManager[] getTrustManagers() {
        return new TrustManager[]{new TrustAllManager()};
    }
}
