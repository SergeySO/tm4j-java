package com.adaptavist.api.utils.exeption;

/**
 *  <strong>W</strong>hat the <strong>T</strong>errible <strong>F</strong>ailure: stream
 *  {@code RuntimeException} was thrown into absolutely incredible situations that should never have happened.
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class WTFException extends RuntimeException {
    private static final String WHAT_THE_TERRIBLE_FAILURE = "What the Terrible Failure! ";

    /**
     * @see WTFException
     */
    public WTFException() {
        super(WHAT_THE_TERRIBLE_FAILURE);
    }

    /**
     * @see WTFException
     */
    public WTFException(String detailMessage, Throwable throwable) {
        super(WHAT_THE_TERRIBLE_FAILURE + detailMessage, throwable);
    }

    /**
     * @see WTFException
     */
    public WTFException(String detailMessage) {
        super(WHAT_THE_TERRIBLE_FAILURE + detailMessage);
    }

    /**
     * @see WTFException
     */
    public WTFException(Throwable throwable) {
        super(WHAT_THE_TERRIBLE_FAILURE, throwable);
    }
}
