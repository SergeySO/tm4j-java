package com.adaptavist.api;

import com.adaptavist.model.TestResultRq;
import com.adaptavist.model.TestResultRs;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * @author Osinnikov S.S. on 27.12.2021
 */
public interface TestResultService {
    @POST("atm/1.0/testrun/{testRunKey}/testcase/{testKey}/testresult")
    Call<TestResultRs> createTestResult(@Path("testRunKey") String testRunKey,
                                        @Path("testKey") String testKey,
                                        @Body TestResultRq testResultRq);

    @Multipart
    @POST("atm/1.0/testresult/{testResultId}/step/{stepIndex}/attachments")
    Call<Void> addAttachmentToTestCaseResult(@Path("testResultId") String testResultId,
                                             @Path("stepIndex") String stepIndex,
                                             @Part MultipartBody.Part filePart);
}
