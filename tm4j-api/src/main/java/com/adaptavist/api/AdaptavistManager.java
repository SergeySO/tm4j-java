package com.adaptavist.api;

import com.adaptavist.model.Credential;
import com.adaptavist.model.SessionData;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import java.io.IOException;

/**
 * @author Osinnikov S.S. on 28.12.2021
 */
public class AdaptavistManager {
    private final AuthorizationService authorizationService;
    private final TestCaseService testCaseService;
    private final TestResultService testResultService;
    private final TestRunService testRunService;

    public AdaptavistManager(String baseUrl) {
        Retrofit retrofit = APIClient.getClient(baseUrl);
        authorizationService = retrofit.create(AuthorizationService.class);
        testCaseService = retrofit.create(TestCaseService.class);
        testResultService = retrofit.create(TestResultService.class);
        testRunService = retrofit.create(TestRunService.class);
    }

    public SessionData connect(@NotNull String login, @NotNull String password) throws IOException {
        Credential credential = new Credential().setUsername(login).setPassword(password);
        Call<SessionData> retrofitCall = authorizationService.login(credential);
        Response<SessionData> response = retrofitCall.execute();

        if (response.code() == 401) {
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }

        return response.body();
    }
}
