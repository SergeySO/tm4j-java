package com.adaptavist.api;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The object that stores information about cookies
 *
 * @author Osinnikov S.S. on 25.11.2021
 */
public class JavaNetCookieJar implements CookieJar {
    private final HashMap<String, Map<String, Cookie>> cookieStore = new HashMap<>();

    @Override
    public void saveFromResponse(HttpUrl url, @NotNull List<Cookie> cookies) {
        if (cookieStore.get(url.host()) == null) {
            Map<String, Cookie> newCookies = new HashMap<>();
            cookies.forEach(t -> newCookies.put(t.name(), t));
            cookieStore.put(url.host(), newCookies);
            System.out.println();
        } else {
            Map<String, Cookie> currentCookies = cookieStore.get(url.host());
            cookies.forEach(t -> currentCookies.put(t.name(), t));
            cookieStore.put(url.host(), currentCookies);
            System.out.println();
        }
    }

    @NotNull
    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        if (cookieStore.get(url.host()) != null) {
            return new ArrayList<>(cookieStore.get(url.host()).values());
        } else {
            return new ArrayList<>();
        }
    }
}
