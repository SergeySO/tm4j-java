package com.adaptavist.api.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Osinnikov S.S. on 25.11.2021
 */
public class GsonHelper {
    private static final Gson gson = initGson();

    private static Gson initGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    public static Gson getGson() {
        return gson;
    }
}
