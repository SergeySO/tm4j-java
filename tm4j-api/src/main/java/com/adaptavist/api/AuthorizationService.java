package com.adaptavist.api;

import com.adaptavist.model.Credential;
import com.adaptavist.model.SessionData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * @author Osinnikov S.S. on 28.12.2021
 */
public interface AuthorizationService {
    @POST("auth/1/session")
    Call<SessionData> login(@Body Credential credential);
}
