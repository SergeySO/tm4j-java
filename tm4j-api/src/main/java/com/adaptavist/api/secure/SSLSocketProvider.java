package com.adaptavist.api.secure;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Provider for TLS entities.
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public interface SSLSocketProvider {

    /**
     * @return factory SSL-sockets.
     */
    SSLSocketFactory getSocketFactory();

    /**
     * @return instance X509TrustManager
     */
    X509TrustManager getX509TrustManager();
}
