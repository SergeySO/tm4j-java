package com.adaptavist.api.secure;

import com.adaptavist.api.utils.SSLUtils;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Stub реализация {@link SSLSocketProvider} для unsecure доступа. Доверяет всем.
 *
 * @author Osinnikov S.S. on 16.11.2021
 */

/**
 * @author Osinnikov S.S. on 16.11.2021
 */
public class StubSSLSocketProvider implements SSLSocketProvider {
    @Override
    public SSLSocketFactory getSocketFactory() {
        return SSLUtils.createSocketFactory();
    }

    @Override
    public X509TrustManager getX509TrustManager() {
        return new TrustAllManager();
    }
}
